### Linha de comando

Após abrir o Git Bash na pasta do projeto, os seguintes comandos podem ser utilizdos.


Criar um repositório local, iniciando o projeto Git
```git init```

Criar link entre o Repositório local e o remoto
```git remote add alias-do-repositório link-do-repositório```


Clonar repoasitório do remoto para o local
```git clone link-do-respositório```

Verificar alterações locais
```git status```

Quando modificar/criar um arquivo do repositório, utilizar
```git add nome-do-arquivo.extensão```

Quando deseja remover um arquivo do repositório, utilizar
```git rm nome-do-arquivo.extensão```

Quando deseja remover um arquivo do repositório

Para adicionar uma referência para a modificação na árvore do git, é necessário dar commit utilizando
```git commit -m "Explicação curta sobre suas modificações, de forma imperativa"```
Caso seja necessário com explicação mais longa utilizar
```git commit -m "explicação curta" -m "explicação mais detalhada das modificações"```

Para requisitar que as modificações no remoto sejam atualizadas no local (caso ajam! Iportante utilizar apenas depois de dar commit em todos os arquivos que sofreram alterações) utilizar
```git pull origin master```


Para enviar suas modificações locais que já possuem commit para o remoto, usar
```git push origin master```
ou ```git push``` caso tenha referenciado o alias como padrão


Ver histórico de modificações (Ver todos os commits, usar teclas de navegação seta cima e seta baixo ou pg up e pg down para percorrer o histórico ou Q para sair)
```git log```


### Usando o .gitignore
Quando é desejado excluir ou rejeitar arquivos do repositório previamente (i.e. arquivos se cache ou de editores de texto) utilizamos o arquivo .gitignore.
i.e.
> .gitignore

> .vscode/

> *.swp

Este arquivo irá ignorar todas as pastas que possuem o nome .vscode e todos os arquivos que tenham a extensão swp.
