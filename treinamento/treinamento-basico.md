# Desafio Web #


## Proposta

A SofTeam assinou um contrato para um projeto à pedido de Mr Needful.

O site será uma galeria de imagens (em anexo). O site terá apenas as seguintes funcionalidades.

 - Cards com dados dos personagens

   - Nome do personagem

   - Foto do personagem

   - Descrição do personagem

   - Poderes, habilidades e Grupos Afiliados

 - Botão para adicionar nova carta:

   - Formulário que recebe os seguintes dados:

    - Nome do personagem

    - Foto do personagem

    - Descrição do personagem

    - Poderes, habilidades e Grupos Afiliados

    - Botões para cancelar e salvar informações.


Obs.: As informações não serão persistentes.


## Instruções

 - Deve ser realizado um clone do repositório [Sertanejo na Web](https://bitbucket.org/netojocelino/sertanejo-na-web)

 - Criar uma pasta com seu nome e ***site-1*** separado por hífen. ***eg:*** _jocelino-neto-site-1/_ ***Apenas os arquivos devem ser modificados***;

 - Dede ser utilizado:

   - *Obrigatoriamente:*

     - HTML5

     - CSS3

     - JavaScript

   - *Opcionalmente:*

      - Bootstrap

      - jQuery



### Prazo

O site tem prazo de 5 dias úteis (15h de desenvolvimento);

### Informações adicionais

Quaisquer informações necessários devem ser enviadas para o email jocelino@softeam.com.br;

Caso o desafio não possa ser realizado deve ser enviado uma justificativa para o email jocelino@softeam.com.br até o dia 10 de Janeiro de 2018;

O inicio do desafio será 9 de Janeiro de 2018 e finalizado 15 de Janeiro de 2018.




Boa sorte.