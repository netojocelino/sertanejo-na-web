# Sertanejo na Web #
###### Um guia de estudos ######


## Links úteis ##
Segue uma lista de links úteis para orientar o caminho a ser seguido para cada área.

* [Roadmap para Frontend, Backend, DevOps](https://github.com/kamranahmedse/developer-roadmap) Uma trilha recomendando tecnologias e ferramentas para estudar na trilha para Backend, Frontend ou DevOps.
* [Roadmap para ReactJS](https://github.com/adam-golab/react-developer-roadmap/) Uma trilha mais aplicada para seguir o caminho de ser desenvolvedor para ReactJS (em muitos pontos segue uma linha parecida com a anterior, para Frontend).
* [Checklist para performace Frontend](https://github.com/thedaviddias/Front-End-Performance-Checklist/) Uma lista para acelerar o frontend com tecnicas que possuem seus *pesos*.



## Referências de estudos ##

Segue uma pequena listagem de materiais para estudos de HTML, CSS, Bootstrap, Git, JavaScript e WordPress.


### Treinamento Pluralsight (Sites) ###

* [Todos os vídeos](https://app.pluralsight.com/channels/details/81f1c6bd-0712-4a0f-b838-fd5b022e1b97) Uma playlist organizada para uma melhor visibilidade de qual caminho seguir.
* [Introduction to Web Development](https://app.pluralsight.com/library/courses/web-development-intro) Introdução ao desenvolvimento web, partindo de HTML, CSS, JS até jQuery.
* [Practical HTML5](https://app.pluralsight.com/library/courses/practical-html5).
* [CSS3](https://app.pluralsight.com/library/courses/css3) Alguns conceitos de CSS3 para efeitos mais robustos.
* [Creating a Responsive Web Design](https://app.pluralsight.com/library/courses/creating-responsive-web-design) Um curso para ensinar alguns conceitos para adaptação do layout dos sites.
* [WordPress Fundamentals: WP_Query and the Loop](https://app.pluralsight.com/library/courses/wordpress-fundamentals-wp-query-loop) Fundamentos de uma função que será bastante usada para a exibição de publicações no WordPress.


### HTML ###

* Documentação [Mozilla Developer Network](https://developer.mozilla.org/pt-BR/docs/Web/HTML)
* Perguntas [Stack OverFlow](https://stackoverflow.com/questions/tagged/html)


### CSS3 ###

* Documentação [Mozilla Developer Network](https://developer.mozilla.org/pt-BR/docs/Web/CSS)
* Perguntas [Stack OverFlow](https://stackoverflow.com/questions/tagged/css)


### Bootstrap ###

* Documentação [Bootstrap 3.3](https://getbootstrap.com/docs/3.3/)


### Git ###

* Arquivo com comandos [básicos](manual-git.md)
* Documentação [Git](https://git-scm.com/docs)
* Perguntas [Stack OverFlow](https://stackoverflow.com/questions/tagged/git)


### JavaScript ###

* Documentação [Mozilla Developer Network](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript)
* Perguntas [Stack OverFlow](https://pt.stackoverflow.com/questions/tagged/javascript)
* **jQuery** [Documentação](http://api.jquery.com/) | [Perguntas](https://stackoverflow.com/questions/tagged/jquery)

### WordPress ###

* Documentação [PHP](http://php.net/docs.php)
* Perguntas [Stack OverFlow](https://stackoverflow.com/questions/tagged/php)
* Documentação [WordPress](http://developer.wordpress.org/)
* Perguntas [Stack OverFlow](https://stackoverflow.com/questions/tagged/wordpress)
